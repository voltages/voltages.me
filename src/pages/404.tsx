import { NextPage } from "next";
import Head from "next/head";
import confetti from 'canvas-confetti';

function fireConfetti() {
    confetti({
        particleCount: 100,
        spread: 70,
        origin: { y: 0.45, x: 0.5 }
    });
}

const NotFound: NextPage = () => {
    if (typeof window !== 'undefined') {
        window.addEventListener('load', fireConfetti);
    }

    return (
        <>
            <Head>
                <title>404 - Voltages</title>
            </Head>
            <div className="max-w-[64em] ml-auto mr-auto">
                <div className="flex flex-col pb-20 px-8">
                    <p className="font-['Fira_Sans'] font-bold text-gray-300 text-3xl">⚠️ 404 - Not Found</p>
                    <p className="font-['Fira_Sans'] font-bold text-gray-300 text-2xl">The thing you were looking for appears to not exist.</p>
                </div>
            </div>
        </>
    );
};

export default NotFound;