import '../styles/globals.css';
import type { AppProps } from 'next/app';
import Head from 'next/head';
import { useLanyardWS } from 'use-lanyard';
import { createContext, Suspense } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import { useRouter } from "next/router";
import { Footer } from '../components/Footer';
import Spotify from '../components/Spotify';

const DISCORD_ID = '218972931701735424' // @Voltages

export const AppContext = createContext<ReturnType<typeof useLanyardWS> | null>(null);

function MyApp({ Component, pageProps }: AppProps) {

  const router = useRouter();
  const lanyard = useLanyardWS(DISCORD_ID);

  return (
  <AppContext.Provider value={lanyard}>
    <Head>
      <meta charSet="utf-8"/>
      <meta name="description" content="University student, amateur developer, IT specialist and an otaku. Currently residing in Estonia."/>
      <meta name="author" content="Voltages"/>
      <meta name="keywords" content="Voltages, v4ltages, development, it, it-specialist, information technology, software, github, gitlab, javascript"/>
      <meta name="viewport" content="width=device-width,initial-scale=1"/>
    </Head>
    <div className="min-h-[100vh] flex justify-center items-center bg-gradient-to-br text-[#4c4f69] from-[#eff1f5] to-[#e6e9ef] dark:from-[#12101F] dark:to-[#1E1D2E] dark:text-white">
      <AnimatePresence exitBeforeEnter>
        <motion.div
          key={router.route}
          initial="initialState"
          animate="animateState"
          exit="exitState"
          transition={{
            duration: 0.25,
            ease: "easeInOut"
          }}
          variants={{
            initialState: {
              height: 0,
              opacity: 0,
              scale: 0.90,
            },
            animateState: {
              height: "auto",
              opacity: 1,
              scale: 1,
            },
            exitState: {
              opacity: 0,
              scale: 0.90,
            },
          }}
        >
          <Component {...pageProps} />
        </motion.div>
      </AnimatePresence>
      <Footer/>
      <Suspense fallback={``}>
        <Spotify/>
      </Suspense>
    </div>
  </AppContext.Provider>
  )
}

export default MyApp
