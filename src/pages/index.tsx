import type { NextPage } from "next";
import Head from "next/head";
import dynamic from 'next/dynamic'
import { Suspense } from 'react'
import profilePic from '/public/avatar.jpg'
const Quotes = dynamic(() => import("../components/Quotes"), {
    ssr: false,
})
const Status = dynamic(() => import("../components/Status"), {
    suspense: true,
})

const Home: NextPage = () => {
    return (
        <>
            <Head>
                Hi there!
                What do you think ur doing here?
                <title>Voltages</title>
                <link rel="shortcut icon" type="image/png" sizes="101x96" href="../public/avatar.jpg?width=101&height=96" />
                <link rel="shortcut icon" type="image/png" sizes="34x32" href="../public/avatar.jpg?width=34&height=32" />
                <link rel="shortcut icon" type="image/png" sizes="17x16" href="../public/avatar.jpg?width=17&height=16" />
            </Head>
            <div className="max-w-[64em] ml-auto mr-auto">
                <div className="flex flex-wrap sm:flex-nowrap items-center pb-20 px-8">
                    <div className="relative shrink-0 pb-4 sm:pl-2 sm:pb-0">
                        <img src={profilePic.src} alt="avatar" className="rounded-full w-28 h-28" />
                        <Suspense fallback={``}>
                            <Status />
                        </Suspense>
                    </div>
                    <div>
                        <p className="sm:pl-4 pb-2 font-[Rubik] font-normal text-2xl">Hey, I&#39;m Voltages <span className="wave">👋🏻</span></p>
                        <p className="sm:pl-4 font-['Fira_Sans'] font-normal text-xl text-[#6c6f85] dark:text-gray-300">University student, amateur developer, IT specialist and an otaku.</p>
                        <Suspense fallback={``}>
                            <Quotes />
                        </Suspense>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Home;
