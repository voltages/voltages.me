import { FC } from "react";
import { useRouter } from "next/router";
import { motion } from "framer-motion"

const NavButtons: FC<{ href: string, setColor: string, children: React.ReactNode; }> = ({ href, setColor, children }) => {
  const router = useRouter();

  return (
    <motion.a href={href}
      className={router.asPath === href ? `flex p-1 items-center drop-shadow-md` : `dark:text-gray-300 text-[#6c6f85] dark:drop-shadow-sm drop-shadow-md flex p-1 items-center`}
    >
      <motion.button
        className="group dark:drop-shadow-sm drop-shadow-md outline-2 outline hover:outline-[#6c6f85] dark:hover:outline-gray-300 outline-transparent flex p-2 rounded-lg gap-2 items-center font-normal text-lg active:scale-[.96] transition-transform"
        style={router.asPath === href ? { backgroundColor: setColor, outlineColor: setColor, color: "#000" } : {}}
      >
        {children}
      </motion.button>
    </motion.a>
  );
}
export default NavButtons;