import { motion } from "framer-motion";
import { FiGithub, FiGitlab } from "react-icons/fi";
import { HiOutlineHomeModern, HiOutlineBookOpen, HiCodeBracket, HiOutlineAtSymbol } from "react-icons/hi2";
import NavButtons from "./NavButtons";

export const Footer = () => {
    return (
        <div className="pl-0 md:pl-4 fixed bottom-0 font-['Fira_Sans'] left-0 w-screen h-20 m-0 flex justify-center md:justify-start flex-row bg-[#dce0e8] dark:bg-[#1E1D2E]/90 z-10 dark:border-solid dark:border-t-[1px] dark:border-[#1E1D2E] dark:backdrop-blur gap-1">
            <NavButtons href="/" setColor="#f5c2e7">
                <HiOutlineHomeModern className="text-2xl" />
                <span className="hidden font-normal group-hover:block">Home</span>
            </NavButtons>
            <NavButtons href="/about/" setColor="#cba6f7">
                <HiOutlineBookOpen className="text-2xl" />
                <span className="hidden font-normal group-hover:block md:block">About</span>
            </NavButtons>
            <NavButtons href="/projects/" setColor="#f38ba8">
                <HiCodeBracket className="text-2xl" />
                <span className="hidden font-normal group-hover:block md:block">Projects</span>
            </NavButtons>
            <NavButtons href="/etc/" setColor="#eba0ac">
                <HiOutlineAtSymbol className="text-2xl" />
                <span className="hidden font-normal group-hover:block md:block">Contact + Other</span>
            </NavButtons>
            <motion.a target="_blank" className="flex p-1 items-center" href="https://github.com/v4ltages" rel="noopener noreferrer">
                <motion.button className="dark:hover:outline-gray-300 hover:outline-[#6c6f85] outline-2 outline outline-transparent group dark:drop-shadow-sm drop-shadow-md dark:text-gray-300 text-[#6c6f85] flex p-2 rounded-lg gap-2 items-center font-normal text-lg transition-all active:scale-[.96]">
                    <FiGithub className="text-2xl stroke-[1.5]" />
                    <span className="hidden font-normal group-hover:block">Github</span>
                </motion.button>
            </motion.a>
            <motion.a target="_blank" className="flex p-1 items-center" href="https://gitlab.com/voltages" rel="noopener noreferrer">
                <motion.button className="dark:hover:outline-gray-300 hover:outline-[#6c6f85] outline-2 outline outline-transparent group dark:drop-shadow-sm drop-shadow-md dark:text-gray-300 text-[#6c6f85] flex p-2 rounded-lg gap-2 items-center font-normal text-lg transition-all active:scale-[.96]">
                    <FiGitlab className="text-2xl stroke-[1.5]" />
                    <span className="hidden font-normal group-hover:block">Gitlab</span>
                </motion.button>
            </motion.a>
        </div>
    );
};
